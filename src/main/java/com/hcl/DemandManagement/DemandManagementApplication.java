package com.hcl.DemandManagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DemandManagementApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(DemandManagementApplication.class, args);	
	}
}

package com.hcl.DemandManagement.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mail_data")
public class MailData
{	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "cus_name")
	private String cusName;
	
	@Column(name = "req_no")
	private String reqNo;
	
	@Column(name = "project")
	private String project;
	
	@Column(name = "project_code")
	private String projectCode;
	
	@Column(name = "subband")
	private String subband;
	
	@Column(name = "designation")
	private String designation;
	
	@Column(name = "experience")
	private String experience;
	
	@Column(name = "req_date")
	private Integer reqDate;
	
	@Column(name = "approval_date")
	private Integer approvalDate;
	
	@Column(name = "initiator")
	private String initiator;
	
	@Column(name = "actionable_position")
	private Integer actionablePosition;
	
	@Column(name = "GEO")
	private String GEO;
	
	@Column(name = "BSDMonth")
	private String BSDMonth;
	
	@Column(name = "date")
	private Date date;
	
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCusName() {
		return cusName;
	}
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getReqNo() {
		return reqNo;
	}
	public void setReqNo(String reqNo) {
		this.reqNo = reqNo;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public String getSubband() {
		return subband;
	}
	public void setSubband(String subband) {
		this.subband = subband;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public Integer getReqDate() {
		return reqDate;
	}
	public void setReqDate(Integer reqDate) {
		this.reqDate = reqDate;
	}
	public Integer getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(Integer approvalDate) {
		this.approvalDate = approvalDate;
	}
	public String getInitiator() {
		return initiator;
	}
	public void setInitiator(String initiator) {
		this.initiator = initiator;
	}
	public Integer getActionablePosition() {
		return actionablePosition;
	}
	public void setActionablePosition(Integer actionablePosition) {
		this.actionablePosition = actionablePosition;
	}
	public String getGEO() {
		return GEO;
	}
	public void setGEO(String gEO) {
		GEO = gEO;
	}
	public String getBSDMonth() {
		return BSDMonth;
	}
	public void setBSDMonth(String bSDMonth) {
		BSDMonth = bSDMonth;
	}
	
	

}

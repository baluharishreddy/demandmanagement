package com.hcl.DemandManagement.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hcl.DemandManagement.entity.MailData;
import com.hcl.DemandManagement.repository.MailDataRepository;

@Component
public class MailClass
{
	@Autowired
	private JavaMailSender sender;

	@Autowired
	MailClass mailClass;
	
	@Autowired
	MailDataRepository mailDataRepository;

	//private final LocalDate currentDate = LocalDate.now();
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	private final String first=dateFormat.format(new Date().getTime());
	public static int flag = 0;

	@Scheduled(cron = "0 * * * * ?")
	public String callMail()
	{

		MailData mailData=mailDataRepository.findTopByOrderByIdDesc();
		
		//long updMin = Period.between(currentDate, LocalDate.now()).get(ChronoUnit.MINUTES);
		String updTime=dateFormat.format(new Date().getTime());
		int min=Integer.parseInt(first.substring(14,16));
        int updMin=Integer.parseInt(updTime.substring(14,16));

		System.out.println(min + "------------" + updMin);

		String subject = null, text1 = null, text2 = null, message = null;

		if (updMin-min == 1)
		{
			System.out.println("\n--------------------1st MAIL--------------------");
			subject = "SR’s Without Budget for Account : "+mailData.getCusName()+" as of "+first+" ";
			text1 = "Following SR’s  has been raised against your account and doesn’t have Budget";
			text2 = "Pls review the SR list and kindly update the Budget if valid demand or close the SR.";
			message = mailClass.sendMail(mailData, subject, text1, text2);
		}
		if (updMin-min == 3)
		{
			System.out.println("\n--------------------2nd MAIL--------------------");
			subject = "ESCALATION - SR’s Without Budget for Account : " + mailData.getCusName() + " as of " + updTime + "";
			text1 = "Following SR’s has been raised against your account and doesn’t have Budget. Reminder mails has been sent to your team and has not been actioned.";
			text2 = "Pls review the SR list and kindly update the Budget if valid demand or close the SR.";
			message = mailClass.sendMail(mailData, subject, text1, text2);
		}

		if (updMin-min == 5)
		{
			System.out.println("\n--------------------3rd MAIL--------------------");
			subject = ": SR CLOSURE - SR’s Without Budget for Account:  " + mailData.getCusName() + " as of " + updTime + "";
			text1 = "Following SR’s doesn’t have Budget and needs to be closed as the budget has not been updated after repeated reminders.";
			message = mailClass.sendMail(mailData, subject, text1, null);
		}
		return message;
	}

	
	public String sendMail(MailData mailData, String subject, String text1, String text2) 
	{
		
		flag = 1;
		System.out.println(text1);
		MimeMessage mailMessage = sender.createMimeMessage();
		MimeMessageHelper helper = null;

		try
		{
			helper = new MimeMessageHelper(mailMessage, true);

			helper.setTo("baluharishreddy.m@gmail.com");
			helper.setSubject(subject);
			helper.setText(text1
					+ "\n\n<html><head><style>table {border-collapse: collapse;border: 1px solid black;width: 50%;} td, th {border: 1px solid black;font-style: italic;}th {background-color: #eee6ff;color: black;}</style></head><body><br/><table><tr><th>ReqNo</th> <th>Project</th> <th>Project Code</th> <th>Sub Band</th> <th>Designation</th> <th>Experience</th> <th>Req date</th> <th>Approval Date</th> <th>Initiator</th><th>Actionable Position</th> <th>GEO</th> <th>BSD Month</th></tr><tr> <td>"
					+ mailData.getReqNo() + "</td> <td>" + mailData.getProject() + "</td> <td>"
					+ mailData.getProjectCode() + "</td> <td>" + mailData.getSubband() + "</td> <td>"
					+ mailData.getDesignation() + "</td> <td>" + mailData.getExperience() + "</td> <td>"
					+ mailData.getReqDate() + "</td> <td>" + mailData.getApprovalDate() + "</td> <td>"
					+ mailData.getInitiator() + "</td> <td>" + mailData.getActionablePosition() + "</td> <td>"
					+ mailData.getGEO() + "</td> <td>" + mailData.getBSDMonth()
					+ "</td></tr></table><br/></body></html>\n\n" + text2, true);
			
		} catch (MessagingException e) {
			e.printStackTrace();
			return "Error while sending mail ..";
		}
		sender.send(mailMessage);
		return "Mail Sent Success!";

	}

}

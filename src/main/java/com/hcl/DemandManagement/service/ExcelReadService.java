package com.hcl.DemandManagement.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hcl.DemandManagement.entity.Data;
import com.hcl.DemandManagement.repository.ExcelRepository;

@Service
public class ExcelReadService 
{
	@Autowired
	ExcelRepository excelRepository;

	public String serviceCheck(MultipartFile file)
	{

		try 
		{
			// FileInputStream excelFile = new FileInputStream(file.getInputStream());
			Workbook workbook = new XSSFWorkbook(file.getInputStream());
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = sheet.iterator(); 
			
			Integer col1 = null, col2 = null, col3 = null, col4 = null;
			Row headerRow = sheet.getRow(4);
			for (Cell cell : headerRow) {
				if (cell.getCellTypeEnum() == CellType.STRING) {
					if (cell.getStringCellValue().equals("Customer Code"))
						col1 = cell.getColumnIndex();
					if (cell.getStringCellValue().equals("Requisition Source"))
						col2 = cell.getColumnIndex();
					if (cell.getStringCellValue().equals("Status"))
						col3 = cell.getColumnIndex();
					if (cell.getStringCellValue().equals(" Remarks (as compared with Budget)"))
						col4 = cell.getColumnIndex();
				}
			}

			for (int i = 5; i < sheet.getPhysicalNumberOfRows(); i++)
			{
				Row row = sheet.getRow(i);

				Data data = new Data();

				data.setCustomerCode((int) row.getCell(col1).getNumericCellValue());
				data.setRequisitionSource(row.getCell(col2).getStringCellValue());
				data.setStatus(row.getCell(col3).getStringCellValue());
				data.setRemarks(row.getCell(col4).getStringCellValue());
				excelRepository.save(data);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "Ok";
	}

}

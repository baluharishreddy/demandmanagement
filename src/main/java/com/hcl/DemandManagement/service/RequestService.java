package com.hcl.DemandManagement.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.DemandManagement.entity.Data;
import com.hcl.DemandManagement.entity.MailData;
import com.hcl.DemandManagement.repository.ExcelRepository;
import com.hcl.DemandManagement.repository.MailDataRepository;
import com.hcl.DemandManagement.util.MailClass;


@Service
public class RequestService 
{
	@Autowired
	ExcelRepository excelRepository;
	
	@Autowired(required=false)
	MailClass mailClass;
	
	@Autowired
	MailDataRepository mailDataRepository;
	
	public String serviceCheck(int custNo)
	{
		Data data=excelRepository.findByCustomerCode(custNo);
		String message=null;
		
		Date utilDate=new Date();
				
		try
		{

            FileInputStream file = new FileInputStream("C://Users//maddireddy.r//Documents//Hackathon_Usecase/Demand_Gating _Apps-SI_07th Dec'18_Existing.xlsx");
            Workbook workbook = new XSSFWorkbook(file);
            
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = sheet.iterator();    
                       
            Integer cusRowNo=null,c1=null,c2=null,c3=null,c4=null,c5=null,c6=null,c7=null,c8=null,c9=null,c10=null,c11=null,c12=null,cusNameCol=null;
            Row headerRow = sheet.getRow(4);
            for (Cell cell : headerRow)
            {    
            	if(cell.getCellTypeEnum()==CellType.STRING)
            	{
	                if (cell.getStringCellValue().equals("ReqNo"))
	                    c1 = cell.getColumnIndex();	                
	                if (cell.getStringCellValue().equals("Project"))	                
	                    c2 = cell.getColumnIndex();                
	                if (cell.getStringCellValue().equals("Project Code"))
	                    c3 = cell.getColumnIndex();
	                if (cell.getStringCellValue().equals("Sub band"))
	                    c4 = cell.getColumnIndex(); 
	                if (cell.getStringCellValue().equals("Designation"))
	                    c5 = cell.getColumnIndex();	                
	                if (cell.getStringCellValue().equals("Experience"))	                
	                    c6 = cell.getColumnIndex();                
	                if (cell.getStringCellValue().equals("Req Date"))
	                    c7 = cell.getColumnIndex();
	                if (cell.getStringCellValue().equals("Approval Date"))
	                    c8 = cell.getColumnIndex();
	                if (cell.getStringCellValue().equals("Initiator"))
	                    c9 = cell.getColumnIndex();	                
	                if (cell.getStringCellValue().equals("Actionable Position"))	                
	                    c10 = cell.getColumnIndex();                
	                if (cell.getStringCellValue().equals("GEO"))
	                    c11 = cell.getColumnIndex();
	                if (cell.getStringCellValue().equals("BSD Month"))
	                    c12 = cell.getColumnIndex();
	                
	                if (cell.getStringCellValue().equals("Customer"))
	                    cusNameCol = cell.getColumnIndex();              
	                
            	}
            }
            
            
            for(int i=5;i<sheet.getPhysicalNumberOfRows() ;i++)
            {              
                Row row = sheet.getRow(i);
                for (Cell cell : row)
                	if(cell.getCellTypeEnum()==CellType.NUMERIC)
                		if((int)cell.getNumericCellValue()==custNo)
                			cusRowNo=cell.getRowIndex();                                                        
            }
            
            System.out.println("Cus row no is==="+cusRowNo);
            Row cusRow = sheet.getRow(cusRowNo);
            
            MailData mailData=new MailData();
            mailData.setReqNo(cusRow.getCell(c1).getStringCellValue());
            mailData.setProject(cusRow.getCell(c2).getStringCellValue());
            mailData.setProjectCode(cusRow.getCell(c3).getStringCellValue());
            mailData.setSubband(cusRow.getCell(c4).getStringCellValue());
            mailData.setDesignation(cusRow.getCell(c5).getStringCellValue());
            mailData.setExperience(cusRow.getCell(c6).getStringCellValue());
            mailData.setReqDate((int)cusRow.getCell(c7).getNumericCellValue());
            mailData.setApprovalDate((int)cusRow.getCell(c8).getNumericCellValue());
            mailData.setInitiator(cusRow.getCell(c9).getStringCellValue());
            mailData.setActionablePosition((int)cusRow.getCell(c10).getNumericCellValue());
            mailData.setGEO(cusRow.getCell(c11).getStringCellValue());
            mailData.setBSDMonth(cusRow.getCell(c12).getStringCellValue());
            
            String cusName=cusRow.getCell(cusNameCol).getStringCellValue();
            mailData.setCusName(cusName);
            mailData.setDate(new java.sql.Date(utilDate.getTime()));
            mailDataRepository.save(mailData);
            
            
            System.out.println("Req no is===="+mailData.getReqNo());
            
            if(data.getRemarks().equals("Budget Not Available - Reject All Demands"))
            {
            	message=mailClass.callMail();
            }                   
            else
            {
            	message="You have successfully placed your smart request.";
            	System.out.println(message);
            }
                                                
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
				
		return message;
	}
	

}

package com.hcl.DemandManagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.DemandManagement.service.RequestService;

@RestController
public class RequestController 
{
	@Autowired
	RequestService requestService;

	@PostMapping(value = "/request")
	public String requestCheck(@RequestParam int custNo) 
	{
		System.out.println("Cust no is====" + custNo);

		String str = requestService.serviceCheck(custNo);

		return str;
	}

}

package com.hcl.DemandManagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hcl.DemandManagement.service.ExcelReadService;

@RestController
public class ExcelReadController
{
	@Autowired
	ExcelReadService excelReadService;

	@PostMapping(value = "/excelread")
	public String excelCheck(@RequestPart MultipartFile File) 
	{
		String str = excelReadService.serviceCheck(File);
		return str;
	}

}

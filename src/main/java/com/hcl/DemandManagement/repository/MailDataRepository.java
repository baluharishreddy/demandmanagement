package com.hcl.DemandManagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.DemandManagement.entity.Data;
import com.hcl.DemandManagement.entity.MailData;

public interface MailDataRepository extends JpaRepository<MailData,Integer>
{
	public MailData findTopByOrderByIdDesc();
}

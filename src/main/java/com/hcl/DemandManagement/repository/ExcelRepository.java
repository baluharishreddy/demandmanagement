package com.hcl.DemandManagement.repository;

import org.springframework.stereotype.Repository;
import com.hcl.DemandManagement.entity.Data;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ExcelRepository extends JpaRepository<Data,Integer>
{
	public Data findByCustomerCode(Integer custNo);	
}
